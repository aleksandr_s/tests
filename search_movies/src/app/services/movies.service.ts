import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { Movie } from '../models/movie.interface';
import { APP_CONFIG, IAppConfig } from '../app.config';
import { IMovieHttp } from '../models/http/movie-http.interface';

@Injectable({
    providedIn: 'root'
})
export class MoviesService {
    public changeMovies: ReplaySubject<void> = new ReplaySubject<void>(1);

    constructor(private httpClient: HttpClient, @Inject(APP_CONFIG) private config: IAppConfig) { }

    getMoviesByTitle(s: string): Observable<IMovieHttp> {
        return this.httpClient.get<IMovieHttp>(this.config.apiEndpoint, {
            params: { s }
        });
    }

    getMovieById(i: string): Observable<Movie> {
        return this.httpClient.get<Movie>(this.config.apiEndpoint, {
            params: { i }
        });
    }

}
