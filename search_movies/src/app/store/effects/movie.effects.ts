import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';

import { map, withLatestFrom, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { GetMovie, EMovieActions, GetMovieSuccess, GetMovies, GetMoviesSuccess } from '../actions/movie.actions';
import { MoviesService } from '../../services/movies.service';
import { IAppState } from '../state/app.state';
import { selectMovieList } from '../selectors/movie.selectors';
import { Movie } from '../../models/movie.interface';
import { IMovieHttp } from '../../models/http/movie-http.interface';

@Injectable()
export class MovieEffects {
    @Effect()
    getMovie$ = this.actions$.pipe(
        ofType<GetMovie>(EMovieActions.GetMovie),
        map(action => action.payload),
        switchMap((id) => this.moviesService.getMovieById(id)),
        switchMap((movie: Movie) => of(new GetMovieSuccess(movie || null)))
    );

    @Effect()
    getMovies$ = this.actions$.pipe(
        ofType<GetMovies>(EMovieActions.GetMovies),
        map(action => action.payload),
        switchMap(title => this.moviesService.getMoviesByTitle(title)),
        switchMap((movieHttp: IMovieHttp) => of(new GetMoviesSuccess(movieHttp.Search || null)))
    );

    constructor(private moviesService: MoviesService, private actions$: Actions, private store: Store<IAppState>) {

    }
}