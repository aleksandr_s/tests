import { IAppState } from "../state/app.state";
import { createSelector } from '@ngrx/store';
import { IMovieState } from '../state/movie.state';

const selectMovies = (state: IAppState) => state.movies;

export const selectMovieList = createSelector(
    selectMovies,
    (state: IMovieState) => state.movies
);

export const selectSelectedMovie = createSelector(
    selectMovies,
    (state: IMovieState) => state.selectedMovie
)