import { IAppState } from "../state/app.state";
import { createSelector } from '@ngrx/store';
import { IMovieState } from '../state/movie.state';
import { IMoviesHistoryState } from '../state/movies-history.state';

const selectMoviesHistory = (state: IAppState) => state.moviesHistory;

export const selectMoviesHistoryList = createSelector(
    selectMoviesHistory,
    (state: IMoviesHistoryState) => state.movies
);