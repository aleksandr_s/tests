import { initialMovieState, IMovieState } from '../state/movie.state';
import { MoviesHistoryActions, EMoviesHistoryActions } from '../actions/movies-history.actions';
import { initialMoviesHistoryState, IMoviesHistoryState } from '../state/movies-history.state';
import { Movie } from '../../models/movie.interface';

export const moviesHistoryReducers = (
    state = initialMoviesHistoryState,
    action: MoviesHistoryActions
): IMoviesHistoryState => {
    switch (action.type) {
        case EMoviesHistoryActions.MoviesHistoryPushStateAction: {
            // TODO: необходимость такого функционала неизвестна, поэтому пока побудет здесь
            if (state.movies.find((movie: Movie) => movie.imdbID === action.payload.imdbID)) {
                return state;
            }

            state.movies.unshift(action.payload)
            return {
                ...state
            };
        }

        default:
            return state;
    }
}