import { ActionReducerMap } from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';

import { IAppState } from '../state/app.state';
import { movieReducers } from './movie.reducers';
import { moviesHistoryReducers } from './movies-history.reducers';

export const appReducers: ActionReducerMap<IAppState, any> = {
    router: routerReducer,
    movies: movieReducers,
    moviesHistory: moviesHistoryReducers
};