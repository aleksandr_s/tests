import { Action } from '@ngrx/store';
import { Movie } from '../../models/movie.interface';

export enum EMoviesHistoryActions {
    MoviesHistoryPushStateAction = '[MoviesHistory] Unshift Movies History',
}

export class MoviesHistoryPushStateAction implements Action {
    public readonly type = EMoviesHistoryActions.MoviesHistoryPushStateAction;
    constructor(public payload: Movie) { }
}

export type MoviesHistoryActions = MoviesHistoryPushStateAction;