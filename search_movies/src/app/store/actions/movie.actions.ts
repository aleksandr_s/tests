import { Action } from '@ngrx/store';
import { Movie } from '../../models/movie.interface';

export enum EMovieActions {
    GetMovies = '[Movie] Get Movies',
    GetMoviesSuccess = '[Movie] Get Movies Success',
    GetMovie = '[Movie] Get Movie',
    GetMovieSuccess = '[Movie] Get Movie Success'
}

export class GetMovies implements Action {
    public readonly type = EMovieActions.GetMovies;
    constructor(public payload: string) { }
}

export class GetMoviesSuccess implements Action {
    public readonly type = EMovieActions.GetMoviesSuccess;
    constructor(public payload: Movie[]) { }
}

export class GetMovie implements Action {
    public readonly type = EMovieActions.GetMovie;
    constructor(public payload: string) { }
}

export class GetMovieSuccess implements Action {
    public readonly type = EMovieActions.GetMovieSuccess;
    constructor(public payload: Movie) { }
}

export type MovieActions = GetMovies | GetMoviesSuccess | GetMovie | GetMovieSuccess;