import { Movie } from '../../models/movie.interface';

export interface IMoviesHistoryState {
    movies: Movie[];
}

export const initialMoviesHistoryState: IMoviesHistoryState = {
    movies: []
}