import { RouterReducerState } from '@ngrx/router-store';

import { IMovieState, initialMovieState } from './movie.state';
import { IMoviesHistoryState, initialMoviesHistoryState } from './movies-history.state';

export interface IAppState {
    router?: RouterReducerState;
    movies: IMovieState;
    moviesHistory: IMoviesHistoryState;
}

const initialAppState: IAppState = {
    movies: initialMovieState,
    moviesHistory: initialMoviesHistoryState
}

export function getInitialState(): IAppState {
    return initialAppState;
}