import { Movie } from '../../models/movie.interface';

export interface IMovieState {
    movies: Movie[];
    selectedMovie: Movie;
}

export const initialMovieState: IMovieState = {
    movies: null,
    selectedMovie: null
}