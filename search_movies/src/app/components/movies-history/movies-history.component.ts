import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { IAppState } from '../../store/state/app.state';
import { selectMoviesHistoryList } from '../../store/selectors/movies-history.selectors';
import { Movie } from '../../models/movie.interface';

@Component({
    selector: 'app-movies-history',
    templateUrl: './movies-history.component.html',
    styleUrls: ['./movies-history.component.scss']
})
export class MoviesHistoryComponent {
    historyList$: Observable<Movie[]> = this.store.pipe(select(selectMoviesHistoryList));

    constructor(private store: Store<IAppState>, private router: Router) { }

    selectMovie(movie: Movie) {
        this.router.navigate(['movie', movie.imdbID]);
    }
}
