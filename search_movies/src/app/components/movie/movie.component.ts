import { Component, OnInit, OnDestroy } from '@angular/core';
import { ParamMap, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';

import { Observable, Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';

import { Movie } from '../../models/movie.interface';
import { IAppConfig } from '../../app.config';
import { IAppState } from '../../store/state/app.state';
import { selectSelectedMovie } from '../../store/selectors/movie.selectors';
import { GetMovie } from '../../store/actions/movie.actions';

@Component({
    selector: 'app-movie',
    templateUrl: './movie.component.html'
})
export class MovieComponent implements OnInit, OnDestroy {
    movie$: Observable<Movie> = this.store.pipe(select(selectSelectedMovie));

    onDestroy: Subject<void> = new Subject<void>();

    constructor(private route: ActivatedRoute, private store: Store<IAppState>) { }

    ngOnInit() {
        this.route.paramMap
            .pipe(takeUntil(this.onDestroy))
            .subscribe((params: ParamMap) => {
                this.store.dispatch(new GetMovie(params.get('id')));
            });
    }

    ngOnDestroy() {
        this.onDestroy.next();
        this.onDestroy.complete();
    }
}
