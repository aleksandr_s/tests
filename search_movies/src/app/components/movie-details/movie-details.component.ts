import { Component, Input } from '@angular/core';

import { Movie } from '../../models/movie.interface';
import { Store } from '@ngrx/store';
import { IAppState } from '../../store/state/app.state';
import { MoviesHistoryPushStateAction } from '../../store/actions/movies-history.actions';

@Component({
    selector: 'app-movie-details',
    templateUrl: './movie-details.component.html',
    styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent {
    private _movie: Movie;

    @Input() set movie(value: Movie) {
        this._movie = value;
        if (value) {
            this.store.dispatch(new MoviesHistoryPushStateAction(this.movie));
        }
    }

    get movie(): Movie {
        return this._movie;
    }

    constructor(private store: Store<IAppState>) { }
}
