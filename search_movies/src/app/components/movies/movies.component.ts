import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';

import { switchMap, take, takeUntil } from 'rxjs/operators';

import { IAppState } from '../../store/state/app.state';
import { GetMovies } from '../../store/actions/movie.actions';
import { selectMovieList, selectSelectedMovie } from '../../store/selectors/movie.selectors';
import { Movie } from '../../models/movie.interface';
import { MoviesService } from '../../services/movies.service';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit, OnDestroy {

    movies: Movie[] = [];

    onDestroy: Subject<void> = new Subject<void>();

    constructor(private store: Store<IAppState>, private router: Router, private moviesService: MoviesService) { }

    ngOnInit() {
        this.moviesService.changeMovies
            .pipe(
                takeUntil(this.onDestroy),
                switchMap(() => this.store.pipe(select(selectMovieList), take(1)))
            )
            .subscribe((movies) => this.movies = movies);
    }

    ngOnDestroy() {
        this.onDestroy.next();
        this.onDestroy.complete();
    }
}
