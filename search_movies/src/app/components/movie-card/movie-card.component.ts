import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Movie } from '../../models/movie.interface';

@Component({
    selector: 'app-movie-card',
    templateUrl: './movie-card.component.html',
    styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent {
    @Input() movie: Movie;

    constructor(private router: Router) { }

    goToMovie(id: string) {
        this.router.navigate(['movie', id]);
    }
}
