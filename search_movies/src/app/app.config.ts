import { InjectionToken } from '@angular/core';

export const APP_CONFIG = new InjectionToken('app.config');

export interface IAppConfig {
    apiEndpoint: string;
    apiKey: string;
}

export const AppConfig: IAppConfig = {
    apiEndpoint: 'http://www.omdbapi.com/',
    apiKey: '86779938'
};
