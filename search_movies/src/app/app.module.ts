import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { MovieComponent } from './components/movie/movie.component';
import { appReducers } from './store/reducers/app.reducers';
import { MovieEffects } from './store/effects/movie.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { MoviesService } from './services/movies.service';
import { MoviesComponent } from './components/movies/movies.component';
import { APP_CONFIG, AppConfig } from './app.config';
import { AppInterceptor } from './app.interceptor';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { MoviesHistoryComponent } from './components/movies-history/movies-history.component';

@NgModule({
    declarations: [
        AppComponent,
        MovieCardComponent,
        MovieComponent,
        MoviesComponent,
        MovieDetailsComponent,
        MoviesHistoryComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatAutocompleteModule,
        MatInputModule,
        MatButtonModule,
        MatListModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot([MovieEffects]),
        StoreRouterConnectingModule.forRoot({ stateKey: 'router' })
    ],
    providers: [
        MoviesService,
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
