import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

import { Observable } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';

import { IAppState } from './store/state/app.state';
import { GetMovies } from './store/actions/movie.actions';
import { selectMovieList } from './store/selectors/movie.selectors';
import { Movie } from './models/movie.interface';
import { MoviesService } from './services/movies.service';
import { MoviesHistoryPushStateAction } from './store/actions/movies-history.actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    private static readonly SEARCH_DEBOUNCE = 300;
    private static readonly MIN_COUNT_CHARS = 5;

    movies$: Observable<Movie[]> = this.store.pipe(select(selectMovieList));
    moviesCtrl: FormControl = new FormControl();

    constructor(private store: Store<IAppState>, private router: Router, private moviesService: MoviesService) { }

    ngOnInit() {
        this.moviesCtrl.valueChanges
            .pipe(
                debounceTime(AppComponent.SEARCH_DEBOUNCE),
                filter(title => title && title.length >= AppComponent.MIN_COUNT_CHARS)
            )
            .subscribe(title => this.getMoviesByTitle(title));
    }

    getMoviesByTitle(title: string) {
        this.store.dispatch(new GetMovies(title));
    }

    goToMovies() {
        const currentTitle = this.moviesCtrl.value;
        if (currentTitle < AppComponent.MIN_COUNT_CHARS) {
            return;
        }

        this.getMoviesByTitle(currentTitle);

        this.moviesService.changeMovies.next();
        this.router.navigate(['movies']);
    }

    displayWithTitleSearch(option: Movie) {
        return option && option.Title
    }

    selectMovie(event: MatAutocompleteSelectedEvent) {
        this.router.navigate(['movie', event.option.value.imdbID]);
    }
}
