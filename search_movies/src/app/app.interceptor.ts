import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inject } from '@angular/core';
import { APP_CONFIG, IAppConfig } from './app.config';


export class AppInterceptor implements HttpInterceptor {

    constructor(@Inject(APP_CONFIG) private config: IAppConfig) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(
            request.clone({
                params: request.params.append('apiKey', this.config.apiKey)
            })
        );
    }
}