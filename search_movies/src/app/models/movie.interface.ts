export interface Movie {
    imdbID: string;
    Title: string;
    Year: string;
    Plot: string;
    Poster: string;
}