import { Movie } from '../movie.interface';

export interface IMovieHttp {
    Search: Movie[];
    totalResults: string;
}